/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * backend is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QString>
#include <QStandardPaths>

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QUuid>

#include "backend.h"
#include "zip.h"

Backend::Backend()
{

    QString writtablePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir writtableDir(writtablePath);
    if (!writtableDir.exists())
        writtableDir.mkpath(".");
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(writtablePath + ".db");
    if (!m_db.open())
        qDebug() << "Error: connection with database failed";
}

bool Backend::isDatabaseOpen()
{
    return m_db.isOpen();
}

bool Backend::createDatabaseTable()
{
    QSqlQuery query;
    query.prepare("CREATE TABLE IF NOT EXISTS apps(title TEXT);");
    return query.exec();
}

bool Backend::addBundle(QString title)
{
    QSqlQuery query;
    query.prepare("INSERT INTO apps(title) values (:title)");
    query.bindValue(":title", title);
    return query.exec();
}

bool Backend::removeBundle(QString title)
{
    QSqlQuery query;
    query.prepare("DELETE FROM apps WHERE title = (:title)");
    query.bindValue(":title", title);
    QFile bundlePath(Backend::getBundlePath(title));
    bundlePath.remove();
    return query.exec();
}

QStringList Backend::getAllBundles()
{
    QStringList dosNames;
    QSqlQuery query("SELECT * FROM apps");
    int idTitle = query.record().indexOf("title");
    while (query.next())
    {
        QString title = query.value(idTitle).toString();
        dosNames << title;
    }
    return dosNames;
}

bool Backend::bundleExists(QString title)
{
    QSqlQuery query;
    query.prepare("SELECT title FROM apps WHERE title = (:title)");
    query.bindValue(":title", title);
    if (query.exec())
        return query.next();
    return false;
}

bool Backend::copyBundle(QString fromPath, QString title)
{
    QString writtablePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QFile file(fromPath);
    file.copy(fromPath, writtablePath + "/" + formatName(title, true) + ".jsdos");
}

QString Backend::getBundlePath(QString fromTitle)
{
    QString writtablePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    return writtablePath + "/" + formatName(fromTitle, true) + ".jsdos";
}

QString Backend::formatName(QString originalName, bool toJdos)
{
    if (toJdos)
    {
        originalName.replace(" ", "-");
        return originalName.toLower();
    }
    originalName.replace("-", " ");
    QStringList parts = originalName.split(' ', QString::SkipEmptyParts);
    for (int i = 0; i < parts.size(); ++i)
        parts[i].replace(0, 1, parts[i][0].toUpper());
    return parts.join(" ");
}

bool Backend::isValidBundle(QString bundlePath)
{
    int err = 0;
    std::string pathStd = bundlePath.toStdString();
    const char *jsbundle = pathStd.c_str();
    qDebug() << "Opening " << jsbundle;
    zip *z = zip_open(jsbundle, 0, &err);
    qDebug() << "Open status " << err;
    const char *name = ".jsdos/dosbox.conf";
    struct zip_stat st;
    zip_stat_init(&st);
    err = zip_stat(z, name, 0, &st);
    qDebug() << "Read status " << err;

    return err == 0;
}

QSettings settings(QSettings::UserScope, "udos.nitanmarcel", "udos");

void Backend::setShowDelegateHelper(bool state)
{
    return settings.setValue("showDelegateHelper", state);
}

bool Backend::getShowDelegateHelper()
{
    return settings.value("showDelegateHelper", true).value<bool>();
}


void Backend::setBackupKey(QString key) {
    return settings.setValue("backupKey", key);
}

QString Backend::getBackupKey() {
    return settings.value("backupKey", "").value<QString>();
}

QString Backend::generateBackupKey() {
    return QUuid::createUuid().toString();
}

QString Backend::replaceBetween(QString text, QString replacement, int start, int end) {
    QString textCopy = text;
    textCopy.replace(start, end - start, replacement);
    return textCopy;
}

QString Backend::insertAt(QString text, QString textIn, int pos) {
    QString textCopy = text;
    textCopy.insert(pos, textIn);
    return textCopy;
    
}
