# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the udos.nitanmarcel package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: udos.nitanmarcel\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-24 17:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AddBundlePage.qml:27
msgid "New Bundle"
msgstr ""

#: ../qml/AddBundlePage.qml:38
msgid ""
"uDos requires apps to be bundled using <a href=\"https://dos.zone/studio"
"\">Dos.Zone's Game Studio</a> before being imported.<br>Game-Studio will "
"allow advanced configuration and virtual gamepad mappings.<br>You can also "
"download prebuild bundles from <a href=\"https://gitlab.com/nitanmarcel/"
"udos/-/blob/main/REPOSITORY.md\">here</a>."
msgstr ""

#: ../qml/AddBundlePage.qml:60 ../qml/ImportPage.qml:23
msgid "Import Bundle"
msgstr ""

#: ../qml/AddBundlePage.qml:78
msgid "Game Studio"
msgstr ""

#: ../qml/AddBundlePage.qml:84
msgid "Repository"
msgstr ""

#: ../qml/AddBundlePage.qml:91
msgid "Done"
msgstr ""

#: ../qml/AddBundlePage.qml:117
msgid "jsdos bundle already exists."
msgstr ""

#: ../qml/AddBundlePage.qml:122
msgid ""
"A jsdos bundle with the same name already exists. Please choose another name "
"or delete the old bundle from the main page."
msgstr ""

#: ../qml/AddBundlePage.qml:136
msgid "Invalid Bundle"
msgstr ""

#: ../qml/AddBundlePage.qml:141
msgid ""
"The selected file is not a valid jsdos bundle. Use the <a href=\"https://dos."
"zone/studio\">Game Studio</a> to convert your DOS app in a valid jsdos "
"bundle."
msgstr ""

#: ../qml/Main.qml:97 ../qml/SettingsPage.qml:33
msgid ""
"Run DOS programs in Ubuntu Touch.<br>© Marcel Alexandru Nitan "
"GPLv3<br><br>Powered by <a href=\"https://js-dos.com/\">js-dos</a>"
msgstr ""

#: ../qml/MainPage.qml:40
msgid "No app available. Press the + sign to add new apps."
msgstr ""

#: ../qml/MainPage.qml:49
msgid "Swipe right to delete. Touch to open."
msgstr ""

#: ../qml/SettingsPage.qml:27
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:48
msgid ""
"A backup key will allow you to save the game progress in the cloud so you "
"can restore it later.<br>Do not share the backup key with anyone as the "
"saves can be overwritten."
msgstr ""

#: ../qml/SettingsPage.qml:60
msgid "Enter your backup key."
msgstr ""

#: ../qml/components/InputContextMenu.qml:40
#: ../qml/components/InputToolButtonMenu.qml:31
msgid "Select All"
msgstr ""

#: ../qml/components/InputContextMenu.qml:64
#: ../qml/components/InputToolButtonMenu.qml:38
msgid "Cut"
msgstr ""

#: ../qml/components/InputContextMenu.qml:82
#: ../qml/components/InputToolButtonMenu.qml:50
msgid "Copy"
msgstr ""

#: ../qml/components/InputContextMenu.qml:99
#: ../qml/components/InputToolButtonMenu.qml:59
msgid "Paste"
msgstr ""

#: udos.desktop.in.h:1
msgid "uDos"
msgstr ""
