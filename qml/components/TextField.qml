/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3 as UITK

TextField {
    id: textField
    selectByMouse: true
    persistentSelection: true
    cursorVisible: true

    onSelectedTextChanged: {
        contextMenu.dismiss()
        contextMenu.popup(textField, 0, textField.height)
    }
    onPressAndHold: {
        contextMenu.dismiss()
        contextMenu.popup(textField, 0, textField.height)
    }
    InputContextMenu {
        id: contextMenu
        target: textField
    }
}