/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5
import "./components" as Custom
import QtQuick.Layouts 1.3

import Backend 1.0

ScrollablePage {
    property string importedBundle: ""
    property string bundleName: ""
    title: i18n.tr("New Bundle")
    Column {
        id: column
        spacing: 40
        width: parent.width
        Layout.alignment: Qt.AlignCenter

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: i18n.tr("uDos requires apps to be bundled using <a href=\"https://dos.zone/studio\">Dos.Zone's Game Studio</a> before being imported.<br>Game-Studio will allow advanced configuration and virtual gamepad mappings.<br>You can also download prebuild bundles from <a href=\"https://gitlab.com/nitanmarcel/udos/-/blob/main/REPOSITORY.md\">here</a>.")
            textFormat: Text.RichText
            onLinkActivated: {
                Qt.openUrlExternally(link)
            }
        }

        Frame {
            anchors.horizontalCenter: parent.horizontalCenter
            Column {
                spacing: 20
                Custom.TextField {
                    visible: importedBundle !== ""
                    text: bundleName
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: TextInput.AlignHCenter
                    onTextChanged: {
                        bundleName = text
                    }
                }

                Button {
                    text: i18n.tr("Import Bundle")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        var uploadPage = stackView.push("ImportPage.qml")
                        uploadPage.imported.connect((fileUrl) => {
                            console.log(fileUrl)
                            if (!Backend.isValidBundle(fileUrl.replace("file://", "")))
                                invalidDialog.open()
                            else {
                                importedBundle = fileUrl
                                var fileName = fileUrl.replace(/^.*[\\\/]/, '')
                                bundleName = Backend.formatName(fileName.substr(0, fileName.lastIndexOf(".")), false)
                            }
                        })
                    }
                }

                Button {
                    text: i18n.tr("Game Studio")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: Qt.openUrlExternally("https://dos.zone/studio")
                }

                Button {
                    text: i18n.tr("Repository")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: Qt.openUrlExternally("https://gitlab.com/nitanmarcel/udos/-/blob/main/REPOSITORY.md")
                }

                Button {
                    visible: importedBundle !== ""
                    text: i18n.tr("Done")
                    highlighted: true
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        if (Backend.bundleExists(Backend.formatName(bundleName, false)))
                            bundleExistsDialog.open()
                        else
                        {
                            Backend.addBundle(Backend.formatName(bundleName, false))
                            Backend.copyBundle(importedBundle.replace("file://", ""), Backend.formatName(bundleName, true))
                            dosListChanged()
                            stackView.pop()
                        }
                    }
                }
            }
        }

        Dialog {
            id: bundleExistsDialog

            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            parent: ApplicationWindow.overlay

            modal: true
            title: i18n.tr("jsdos bundle already exists.")
            standardButtons: Dialog.Ok

            Label {
                anchors.fill: parent
                text: i18n.tr("A jsdos bundle with the same name already exists. Please choose another name or delete the old bundle from the main page.")
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
            }
        }

        Dialog {
            id: invalidDialog

            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            parent: ApplicationWindow.overlay

            modal: true
            title: i18n.tr("Invalid Bundle")
            standardButtons: Dialog.Ok

            Label {
                anchors.fill: parent
                text: i18n.tr("The selected file is not a valid jsdos bundle. Use the <a href=\"https://dos.zone/studio\">Game Studio</a> to convert your DOS app in a valid jsdos bundle.")
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
                onLinkActivated: {
                    Qt.openUrlExternally(link)
                }
            }
        }
    }

    function close() {
        stackView.pop()
    }
}
