/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3 as UITK

import Backend 1.0

ApplicationWindow  {
    id: window
    width: 360
    height: 520
    visible: true
    title: "uDos"

    property bool isFullscreenState: false
    onIsFullscreenStateChanged: () => {
        window.showFullScreen(!isFullscreenState)
    }

    signal dosListChanged()

    header: ToolBar {
        visible: !isFullscreenState
        RowLayout {
            anchors.fill: parent
            spacing: 2

            ToolButton {
                spacing: 10
                text: stackView.currentItem.title
                font.weight: Font.Light
                font.pixelSize: 1.414 * units.dp(14.0)
                icon.name: stackView.depth > 1 ? "go-previous" : ""
                icon.height: units.gu(2)
                icon.width: units.gu(2)
                onClicked: if (stackView.depth > 1) stackView.currentItem.close()
            }

            Row {
                visible: stackView.depth <= 1
                Layout.alignment: Qt.AlignRight
                spacing: 2
                ToolButton {
                    icon.name: "add"
                    icon.height: units.gu(2)
                    icon.width: units.gu(2)
                    onClicked: stackView.push("AddBundlePage.qml")
                }
                ToolButton {
                    icon.name: "settings"
                    icon.height: units.gu(2)
                    icon.width: units.gu(2)
                    onClicked: stackView.push("SettingsPage.qml")
                }
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
    }

    Component.onCompleted: function () {
        stackView.push("MainPage.qml")
    }
}
