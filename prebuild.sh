#!/bin/bash

set -e

npm install yarn

export PATH="$(npm bin):$PATH"
EMULATORS_UI_PATH="${ROOT}/tmp/emulators-ui"
JS_DOS_PATH="${ROOT}/tmp/js-dos"

clone () {
    [ -d ${EMULATORS_UI_PATH} ] && rm -rf ${EMULATORS_UI_PATH}
    git clone https://github.com/js-dos/emulators-ui ${EMULATORS_UI_PATH}
    [ -d ${JS_DOS_PATH} ] && rm -rf ${JS_DOS_PATH}
    git clone https://github.com/caiiiycuk/js-dos ${JS_DOS_PATH}
}

apply_patch () {
    dname=$(basename "$PWD")
    if [ -d "${ROOT}/patches/$dname" ]; then
        echo "Applying patches for $dname"
        for patch in ${ROOT}/patches/$dname/*.patch; do
            git apply ${patch}
        done
    fi
}


build () {
    dname=$(basename "$PWD")
    echo "Building $dname"
    yarn
    yarn run eslint . --ext ts,tsx --max-warnings 0
    yarn run gulp
}

cleanup () {
    if [ -d ${ROOT}/tmp ]; then
        rm -rf ${ROOT}/tmp
    fi
}

prepare () {
    cleanup
    mkdir ${ROOT}/tmp
}

prepare

clone

cd ${EMULATORS_UI_PATH}
apply_patch
build
cd ${JS_DOS_PATH}
apply_patch
build

mv ${ROOT}/tmp/js-dos/dist ${ROOT}/www/js-dos
rm -rf ${ROOT}/tmp

cleanup