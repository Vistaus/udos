# uDos

Run DOS programs in Ubuntu Touch. Mainly designed to be used for games.

App logo licensed under Expat/MIT License. Source https://commons.wikimedia.org/wiki/File:Msdos-icon.svg

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/udos.nitanmarcel)
## License

Copyright (C) 2022  Marcel Alexandru Nitan

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
